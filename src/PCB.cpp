#include "PCB.h"
#include "system.h"
#include <dos.h>
#include "SCHEDULE.H"

ID PCB::posIDPCB = 0;

PCB::PCB (StackSize stackSize, Time timeSlice, Thread*& v){
	if ( stackSize > 65536 ) stackSize = 65536; //64*1024
	stackSize /= sizeof(unsigned);
	stek = new unsigned [stackSize];
	myThread = v;

	#ifndef BCC_BLOCK_IGNORE
	stek[stackSize-1] = FP_SEG(myThread);
	stek[stackSize-2] = FP_OFF(myThread);
	stek[stackSize-5] = 0x200; //enable-ovan I bit
	stek[stackSize-6] = FP_SEG(Thread::wrapper);
	stek[stackSize-7] = FP_OFF(Thread::wrapper);
	ss = FP_SEG(stek+stackSize-16); //svi sacuvani registri pri ulasku u interrupt rutinu
	sp = FP_OFF(stek+stackSize-16);
	bp = sp;
	#endif

	if ( timeSlice == 0 ) { beskonacno = 1; kvant = defaultTimeSlice; }
	else { beskonacno = 0; kvant = timeSlice; }
	stanje = NOV;
	IDPCB = posIDPCB++;

	listaBlokiranih = new List();
	System::PCBs->dodaj(this, IDPCB);
}

PCB::~PCB(){ //waitToComplete(); u izvedenim klasama
	System::PCBs->izbaciTek();
	delete[] stek;
	delete listaBlokiranih;
	myThread = 0;
}

void PCB::start(){
	if ( stanje==NOV ){
		stanje = SPREMAN;
		if ( stanje==SPREMAN && this!=System::idlePCB && this!=System::runningKernelPCB ) Scheduler::put(this);
	}
}

void PCB::waitToComplete(){
	if ( stanje!=NOV && stanje!=ZAVRSIO && this!=System::startPCB && listaBlokiranih!=0 ){
		System::running->stanje = BLOKIRAN;
		listaBlokiranih->dodaj(System::running);
		System::zahtevana_promena_konteksta = 1;
	}
}

void PCB::sleep(Time timeToSleep){
	if ( timeToSleep != 0 && System::listaUspavanih!=0 && System::running->stanje==SPREMAN ){
		System::running->stanje = BLOKIRAN;
		System::listaUspavanih->dodaj(System::running, timeToSleep);
		System::zahtevana_promena_konteksta = 1;
	}
}
