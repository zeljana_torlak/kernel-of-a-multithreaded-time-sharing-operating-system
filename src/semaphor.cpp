#include "semaphor.h"
#include "system.h"
#include <dos.h>

unsigned int stcx, stdx;
volatile int x;

Semaphore::Semaphore (int init){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = SemConst;
	newData->in = init;
	newData->sem = this;
#ifndef BCC_BLOCK_IGNORE
	stcx = FP_SEG(newData);
	stdx = FP_OFF(newData);
#endif
	asm {
		push cx
		push dx
		mov cx, stcx
		mov dx, stdx
		int 61h
	}
	id = newData->id;
	asm {
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
}

Semaphore::~Semaphore (){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = SemDest;
	newData->id = id;
#ifndef BCC_BLOCK_IGNORE
	stcx = FP_SEG(newData);
	stdx = FP_OFF(newData);
#endif
	asm {
		push cx
		push dx
		mov cx, stcx
		mov dx, stdx
		int 61h
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
}

int Semaphore::wait (int toBlock){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = SemWait;
	newData->id = id;
	newData->in = toBlock;
#ifndef BCC_BLOCK_IGNORE
	stcx = FP_SEG(newData);
	stdx = FP_OFF(newData);
#endif
	asm {
		push cx
		push dx
		mov cx, stcx
		mov dx, stdx
		int 61h
	}
	x = newData->ret;
	asm {
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
	return x;
}

void Semaphore::signal(){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = SemSignal;
	newData->id = id;
#ifndef BCC_BLOCK_IGNORE
	stcx = FP_SEG(newData);
	stdx = FP_OFF(newData);
#endif
	asm {
		push cx
		push dx
		mov cx, stcx
		mov dx, stdx
		int 61h
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
}

int Semaphore::val () const{
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = SemVal;
	newData->id = id;
#ifndef BCC_BLOCK_IGNORE
	stcx = FP_SEG(newData);
	stdx = FP_OFF(newData);
#endif
	asm {
		push cx
		push dx
		mov cx, stcx
		mov dx, stdx
		int 61h
	}
	x = newData->ret;
	asm {
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
	return x;
}
