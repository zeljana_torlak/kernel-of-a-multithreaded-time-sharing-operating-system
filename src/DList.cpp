#include "DList.h"

void DList::isprazni(){
	while (prvi){
		stari = prvi;
		prvi = prvi->sled;
		delete stari;
	}
	posl = tek = preth = 0;
	stari = novi = p = q = 0;
}

void DList::dodaj(PCB*& pcb, Time t){
	novi = new Elem(pcb,t);
		if (!prvi) posl = prvi = novi;
		else if ( prvi->vreme > t ){
			prvi->vreme -= t;
			novi->sled = prvi;
			prvi = novi;
		}
		else {
			p = prvi;
			q = 0;
			while ( p && p->vreme <= t ){
				t -= p->vreme;
				q = p; p = p->sled;
			}
			//q nece biti null jer su ti slucajevi obradjeni pocetnim if-ovima
			novi->vreme = t;
			novi->sled = p;
			q->sled = novi;
			if (p) p->vreme -= t;
			else posl = novi;
		}
}

