#include "system.h"

//#include <iostream.h>
//#include <stdlib.h> //exit

extern int userMain (int argc, char* argv[]);

int main(int argc, char* argv[]){
	System::init_System();

	int ret = userMain (argc, argv);

	System::restore_System();

 	return ret;
}
