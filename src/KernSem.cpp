#include "KernSem.h"
#include "system.h"
#include "SCHEDULE.H"

ID KernelSem::posIDSem = 0;

volatile PCB* tek;

KernelSem::KernelSem (int init, Semaphore*& s){
	value = init;
	mySem = s;
	IDSem = ++posIDSem;
	listaCekanja = new List();
	System::KernelSems->dodaj(this, IDSem);
}

KernelSem::~KernelSem (){
	System::KernelSems->izbaciTek();
	for (listaCekanja->naPrvi(); listaCekanja->imaTek(); ){
		tek = listaCekanja->dohvTekPod();
		tek->stanje = SPREMAN;
		listaCekanja->izbaciTek();
		if ( tek->stanje==SPREMAN && tek!=System::idlePCB && tek!=System::runningKernelPCB ) Scheduler::put((PCB*)tek);
	}
	delete listaCekanja;
	mySem = 0;
}

int KernelSem::wait (int toBlock){
	if ( toBlock!=0 ){
		if (--value<0){
			System::running->stanje = BLOKIRAN;
			listaCekanja->dodaj(System::running);
			System::zahtevana_promena_konteksta = 1;
			return 1;
		} else return 0;
	} else {
		if ( value <= 0 ){
			return -1;
		} else {
			value--;
			return 0;
		}
	}
}

void KernelSem::signal(){
	if (value++<0) {
		listaCekanja->naPrvi();
		if ( listaCekanja->imaTek()){
			tek = listaCekanja->dohvTekPod();
			tek->stanje = SPREMAN;
			listaCekanja->izbaciTek();
			if ( tek->stanje==SPREMAN && tek!=System::idlePCB && tek!=System::runningKernelPCB ) Scheduler::put((PCB*)tek);
		}
	}
}

int KernelSem::val () const{
	return value;
}
