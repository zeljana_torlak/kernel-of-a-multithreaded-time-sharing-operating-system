#include "thread.h"
#include "system.h"
#include "SCHEDULE.H"
#include <dos.h>

unsigned int ttcx, ttdx;
volatile PCB* runningPCB;
volatile PCB* pom;

Thread::Thread (StackSize stackSize, Time timeSlice){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	if ( System::special != creatingKernelThread ){
		newData->name = ThrConst;
		newData->stSize = stackSize;
		newData->time = timeSlice;
		newData->thr = this;
	#ifndef BCC_BLOCK_IGNORE
		ttcx = FP_SEG(newData);
		ttdx = FP_OFF(newData);
	#endif
		asm {
			push cx
			push dx
			mov cx, ttcx
			mov dx, ttdx
			int 61h
		}
		id = newData->id;
		asm {
			pop dx
			pop cx
		}
	} else {
		System::runningKernelPCB = new PCB(stackSize, timeSlice, this);
		id = System::runningKernelPCB->IDPCB;
		System::special = nothing;
	}
	delete newData;
	asm popf;
}

void Thread::start(){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = ThrStart;
	newData->id = id;
#ifndef BCC_BLOCK_IGNORE
	ttcx = FP_SEG(newData);
	ttdx = FP_OFF(newData);
#endif
	asm {
		push cx
		push dx
		mov cx, ttcx
		mov dx, ttdx
		int 61h
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
}

void Thread::waitToComplete(){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = ThrWTC;
	newData->id = id;
#ifndef BCC_BLOCK_IGNORE
	ttcx = FP_SEG(newData);
	ttdx = FP_OFF(newData);
#endif
	asm {
		push cx
		push dx
		mov cx, ttcx
		mov dx, ttdx
		int 61h
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
}

Thread::~Thread(){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	if ( System::special != destroyingKernelThread ){
		newData->name = ThrDest;
		newData->id = id;
	#ifndef BCC_BLOCK_IGNORE
		ttcx = FP_SEG(newData);
		ttdx = FP_OFF(newData);
	#endif
		asm {
			push cx
			push dx
			mov cx, ttcx
			mov dx, ttdx
			int 61h
			pop dx
			pop cx
		}
	} else {
		delete System::runningKernelPCB;
		System::special = nothing;
	}
	delete newData;
	asm popf;
}

void Thread::sleep(Time timeToSleep){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = ThrSleep;
	newData->time = timeToSleep;
#ifndef BCC_BLOCK_IGNORE
	ttcx = FP_SEG(newData);
	ttdx = FP_OFF(newData);
#endif
	asm {
		push cx
		push dx
		mov cx, ttcx
		mov dx, ttdx
		int 61h
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
}

void Thread::wrapper(Thread* runningThr){
	if ( runningThr!=0 ) runningThr->run();
	// other system calls...?
	asm pushf;
	asm cli;
	System::PCBs->nadjiPCB(runningThr);
	runningPCB = (PCB*)System::PCBs->dohvTekPod();
	if ( runningPCB!=0 && runningPCB->listaBlokiranih!=0 )
	for(runningPCB->listaBlokiranih->naPrvi(); runningPCB->listaBlokiranih->imaTek(); ){
			pom = runningPCB->listaBlokiranih->dohvTekPod();
			runningPCB->listaBlokiranih->izbaciTek();
			pom->stanje = SPREMAN;
			if ( pom->stanje==SPREMAN && pom!=System::idlePCB && pom!=System::runningKernelPCB ) Scheduler::put((PCB*)pom);
		}
	runningPCB->stanje = ZAVRSIO;
	asm popf;

	dispatch();
}

void dispatch(){ // sinhrona promena konteksta
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = ThrDispatch;
	#ifndef BCC_BLOCK_IGNORE
		ttcx = FP_SEG(newData);
		ttdx = FP_OFF(newData);
	#endif
	asm {
		push cx
		push dx
		mov cx, ttcx
		mov dx, ttdx
		int 61h
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
}
