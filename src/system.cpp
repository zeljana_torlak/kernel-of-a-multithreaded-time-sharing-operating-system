#include "system.h"
#include <dos.h>
#include "SCHEDULE.H"

extern void tick();

volatile unsigned tsp, tss, tbp, tcx, tdx;
volatile PCB* tekPCB;

PCB* System::running = 0;
KernelThread* System::runningKernelThread = 0;
PCB* System::runningKernelPCB = 0;

Idle* System::idle = 0;
PCB* System::idlePCB = 0;
PCB* System::startPCB = 0;
DList* System::listaUspavanih = 0;

SystemData* System::SData = 0;
AList* System::PCBs = 0;
AList* System::KernelSems = 0;
AList* System::KernelEvs = 0;

Special System::special = nothing;
unsigned int System::lockFlag = 1;
unsigned int System::zahtevana_promena_konteksta = 0;
int System::brojac = defaultTimeSlice;
pInterrupt System::oldRout = 0;

void System::init_System(){
	lock;
#ifndef BCC_BLOCK_IGNORE
	oldRout = getvect(0x08);
	setvect(0x08, timer);
	setvect(0x60, oldRout);
	setvect(0x61, SysCallRout);
	setvect(0x63, change);
#endif

	PCBs = new AList();
	KernelSems = new AList();
	KernelEvs = new AList();
	listaUspavanih = new DList();

	Thread* startThr = 0;
	running = new PCB(1024, defaultStackSize, startThr);
	running->stanje = SPREMAN;
	startPCB = System::running;

	special = creatingKernelThread;
	runningKernelThread = new KernelThread(5120);
	runningKernelThread->start();

	special = creatingIdle;
	idle = new Idle(1024);
	idle->start();
	unlock;
}

void System::restore_System(){
	lock;
	delete idle;
	System::special = destroyingKernelThread;
	delete runningKernelThread;
	//delete startPCB; //running
	delete listaUspavanih;
	delete KernelEvs;
	delete KernelSems;
	delete PCBs;
#ifndef BCC_BLOCK_IGNORE
	setvect(0x08, oldRout);
#endif
	unlock;
}

void interrupt timer(...){

	if ( System::running->beskonacno == 0 ) System::brojac--;

	if ( System::listaUspavanih!=0 && System::listaUspavanih->prvi!=0 ) {
		System::listaUspavanih->prvi->vreme--;
		for(System::listaUspavanih->naPrvi(); System::listaUspavanih->imaTek() && System::listaUspavanih->tek->vreme==0; ){
			tekPCB = System::listaUspavanih->dohvTekPod();
			System::listaUspavanih->izbaciTek();
			tekPCB->stanje = SPREMAN;
			if ( tekPCB->stanje==SPREMAN && tekPCB!=System::idlePCB && tekPCB!=System::runningKernelPCB ) Scheduler::put((PCB*)tekPCB);
		}
		System::listaUspavanih->naPrvi();
	}
	tick();

	if ( (System::brojac==0 && System::running->beskonacno==0) || System::zahtevana_promena_konteksta ) {
		if ( System::lockFlag == 1 ){
			System::zahtevana_promena_konteksta = 0;

			asm {
				mov tsp, sp
				mov tss, ss
				mov tbp, bp
			}

			System::running->sp = tsp;
			System::running->ss = tss;
			System::running->bp = tbp;

			if ( System::running->stanje==SPREMAN && System::running!=System::idlePCB && System::running!=System::runningKernelPCB ) Scheduler::put(System::running);
			System::running = Scheduler::get();
			if ( System::running==0 ) System::running = System::idlePCB;

			tsp = System::running->sp;
			tss = System::running->ss;
			tbp = System::running->bp;

			System::brojac = System::running->kvant;

			asm {
				mov sp, tsp
				mov ss, tss
				mov bp, tbp
			}

		} else System::zahtevana_promena_konteksta = 1;
	}

	asm int 60h;
}

void interrupt SysCallRout(...){
	unlock;
	System::lockFlag = 0;

	asm { //cuvanje konteksta tekuce korisnicke niti
		mov tsp, sp
		mov tss, ss
		mov tbp, bp
	}

	System::running->sp = tsp;
	System::running->ss = tss;
	System::running->bp = tbp;

	asm { //dovatanje parametara sistemskog poziva
		mov tcx, cx
		mov tdx, dx
	}

#ifndef BCC_BLOCK_IGNORE
	System::SData = (SystemData*)MK_FP(tcx,tdx);
#endif

	tsp = System::runningKernelPCB->sp; //prelazak na kontekst interne kernel niti
	tss = System::runningKernelPCB->ss;
	tbp = System::runningKernelPCB->bp;

	asm {
		mov sp, tsp
		mov ss, tss
		mov bp, tbp
	}

	//lock;
}

void interrupt change(...){
	unlock;

	asm { //cuvanje konteksta interne kernel niti
		mov tsp, sp
		mov tss, ss
		mov tbp, bp
	}

	System::runningKernelPCB->sp = tsp;
	System::runningKernelPCB->ss = tss;
	System::runningKernelPCB->bp = tbp;

	if ( System::zahtevana_promena_konteksta ){

		if ( System::running->stanje==SPREMAN && System::running!=System::idlePCB && System::running!=System::runningKernelPCB ) Scheduler::put(System::running);
		System::running = Scheduler::get();
		if ( System::running==0 ) System::running = System::idlePCB;

	}

	tsp = System::running->sp; //prelazak na kontekst tekuce korisnicke niti
	tss = System::running->ss;
	tbp = System::running->bp;

	if ( System::zahtevana_promena_konteksta ) System::brojac = System::running->kvant;
	System::zahtevana_promena_konteksta = 0;

	asm {
		mov sp, tsp
		mov ss, tss
		mov bp, tbp
	}

	System::lockFlag = 1;
	lock;
}

