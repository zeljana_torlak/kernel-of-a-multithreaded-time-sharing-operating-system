#include "KernEv.h"
#include "system.h"
#include "SCHEDULE.H"

KernelEv* KernelEv::kernelEvents[256] = {0};
ID KernelEv::posIDEv = 0;

KernelEv::KernelEv (IVTNo N, Event*& e){
	ivtNo = N;
	myEvent = e;
	vlasnik = System::running;
	value = 1;
	blokiranPCB = 0;
	kernelEvents[N] = this;
	IDEv = ++posIDEv;
	System::KernelEvs->dodaj(this, IDEv);
}

KernelEv::~KernelEv (){
	System::KernelEvs->izbaciTek();
	if ( blokiranPCB!=0 ){
		blokiranPCB->stanje = SPREMAN;
		if ( blokiranPCB->stanje==SPREMAN && blokiranPCB!=System::idlePCB && blokiranPCB!=System::runningKernelPCB ) Scheduler::put(blokiranPCB);
		blokiranPCB = 0;
	}
	kernelEvents[ivtNo] = 0;
	vlasnik = 0;
	myEvent = 0;
}

void KernelEv::wait (){
	if ( System::running == vlasnik ){
		if ( value == 1 ) value = 0;
		else{
			System::running->stanje = BLOKIRAN;
			blokiranPCB = System::running;
			System::zahtevana_promena_konteksta = 1;
		}
	}
}

void KernelEv::signal(){
	if ( blokiranPCB == 0 ) value = 1;
	else {
		blokiranPCB->stanje = SPREMAN;
		if ( blokiranPCB->stanje==SPREMAN && blokiranPCB!=System::idlePCB && blokiranPCB!=System::runningKernelPCB ) Scheduler::put(blokiranPCB);
		blokiranPCB = 0;
		if (System::lockFlag == 1) dispatch();
		else System::zahtevana_promena_konteksta = 1;
		}
}
