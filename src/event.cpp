#include "event.h"
#include "system.h"
#include <dos.h>

unsigned int etcx, etdx;

Event::Event (IVTNo ivtNo){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = EvConst;
	newData->numEnt = ivtNo;
	newData->ev = this;
#ifndef BCC_BLOCK_IGNORE
	etcx = FP_SEG(newData);
	etdx = FP_OFF(newData);
#endif
	asm {
		push cx
		push dx
		mov cx, etcx
		mov dx, etdx
		int 61h
	}
	id = newData->id;
	asm {
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
}

Event::~Event (){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = EvDest;
	newData->id = id;
#ifndef BCC_BLOCK_IGNORE
	etcx = FP_SEG(newData);
	etdx = FP_OFF(newData);
#endif
	asm {
		push cx
		push dx
		mov cx, etcx
		mov dx, etdx
		int 61h
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
}

void Event::wait (){
	asm pushf;
	asm cli;
	SystemData* newData = new SystemData();
	newData->name = EvWait;
	newData->id = id;
#ifndef BCC_BLOCK_IGNORE
	etcx = FP_SEG(newData);
	etdx = FP_OFF(newData);
#endif
	asm {
		push cx
		push dx
		mov cx, etcx
		mov dx, etdx
		int 61h
		pop dx
		pop cx
	}
	delete newData;
	asm popf;
}

void Event::signal(){
	asm pushf;
	asm cli;
	if ( System::KernelEvs!=0 && System::KernelEvs->nadji(id) ) ((KernelEv*)System::KernelEvs->dohvTekPod())->signal();
	asm popf;
}
