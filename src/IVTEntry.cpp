#include "IVTEntry.h"
#include "system.h"
#include <dos.h>

IVTEntry::IVTEntry(IVTNo numEntr, pInterrupt interruptRoutine){
	asm pushf;
	asm cli;
	numEntry = numEntr;
#ifndef BCC_BLOCK_IGNORE
	oldRoutine = getvect(numEntr);
	setvect(numEntr, interruptRoutine);
#endif
	asm popf;
}

IVTEntry::~IVTEntry(){
	asm pushf;
	asm cli;
#ifndef BCC_BLOCK_IGNORE
	setvect(numEntry, oldRoutine);
#endif
	asm popf;
}

void IVTEntry::callOld(){
	asm pushf;
	asm cli;
	if (oldRoutine) oldRoutine();
	asm popf;
}

void IVTEntry::signal(){
	asm pushf;
	asm cli;
	if (KernelEv::kernelEvents[numEntry]) KernelEv::kernelEvents[numEntry]->signal();
	asm popf;
}
