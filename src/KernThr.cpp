#include "KernThr.h"
#include "system.h"

PCB* KernelThread::a = 0;
KernelSem* KernelThread::b = 0;
KernelEv* KernelThread::c = 0;

void KernelThread::run(){
	while(1){
		unlock;

		switch (System::SData->name){
		case ThrDispatch: {
			System::zahtevana_promena_konteksta = 1;
			break;
		}
		case ThrConst: {
			a = new PCB(System::SData->stSize, System::SData->time, System::SData->thr);
			System::SData->id = a->IDPCB;
			if ( System::special==creatingIdle ) {System::idlePCB = a; System::special = nothing;}
			break;
		}
		case ThrDest: {
			if ( System::PCBs!=0 && System::PCBs->nadji(System::SData->id) ) delete ((PCB*)System::PCBs->dohvTekPod());
			break;
		}
		case ThrStart: {
			if ( System::PCBs!=0 && System::PCBs->nadji(System::SData->id) ) ((PCB*)System::PCBs->dohvTekPod())->start();
			break;
		}
		case ThrWTC: {
			if ( System::PCBs!=0 && System::PCBs->nadji(System::SData->id) ) ((PCB*)System::PCBs->dohvTekPod())->waitToComplete();
			break;
		}
		case ThrSleep: {
			PCB::sleep(System::SData->time);
			break;
		}
		case SemConst: {
			b = new KernelSem(System::SData->in, System::SData->sem);
			System::SData->id = b->IDSem;
			break;
		}
		case SemDest: {
			if ( System::KernelSems!=0 && System::KernelSems->nadji(System::SData->id) ) delete ((KernelSem*)System::KernelSems->dohvTekPod());
			break;
		}
		case SemWait: {
			if ( System::KernelSems!=0 && System::KernelSems->nadji(System::SData->id) ) System::SData->ret = ((KernelSem*)System::KernelSems->dohvTekPod())->wait(System::SData->in);
			break;
		}
		case SemSignal: {
			if ( System::KernelSems!=0 && System::KernelSems->nadji(System::SData->id) ) ((KernelSem*)System::KernelSems->dohvTekPod())->signal();
			break;
		}
		case SemVal: {
			if ( System::KernelSems!=0 && System::KernelSems->nadji(System::SData->id) ) System::SData->ret = ((KernelSem*)System::KernelSems->dohvTekPod())->val();
			break;
		}
		case EvConst: {
			c = new KernelEv(System::SData->numEnt, System::SData->ev);
			System::SData->id = c->IDEv;
			break;
		}
		case EvDest: {
			if ( System::KernelEvs!=0 && System::KernelEvs->nadji(System::SData->id) ) delete ((KernelEv*)System::KernelEvs->dohvTekPod());
			break;
		}
		case EvWait: {
			if ( System::KernelEvs!=0 && System::KernelEvs->nadji(System::SData->id) ) ((KernelEv*)System::KernelEvs->dohvTekPod())->wait();
			break;
		}
		}

		lock;
		asm int 63h;
	}
}

