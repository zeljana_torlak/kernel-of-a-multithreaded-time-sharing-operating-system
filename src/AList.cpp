#include "AList.h"
#include "PCB.h"

void AList::isprazni(){
	while (prvi){
		stari = prvi;
		prvi = prvi->sled;
		delete stari;
	}
	posl = tek = preth = 0;
	stari = 0;
}

unsigned AList::nadji(ID i){
	if ( !imaTek() ) naPrvi();
	else if ( i < tek->IDElem ) naPrvi();
	for (; imaTek() && tek->IDElem < i; naSled());
	if ( imaTek() && tek->IDElem == i ) return 1;
	else return 0;
}

unsigned AList::nadjiPCB(Thread*& t){
	if ( imaTek() && ((PCB*)tek->pod)->myThread == t ) return 1;
	for (naPrvi(); imaTek(); naSled())
		if ( ((PCB*)tek->pod)->myThread == t ) return 1;
	return 0;
}
