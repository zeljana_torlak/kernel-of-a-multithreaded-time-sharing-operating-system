#ifndef _list_h_
#define _list_h_

class PCB;

class List {
public:

	struct Elem {
		PCB* pod;
		Elem* sled;
		Elem (PCB*& p,  Elem* s = 0)
			{ pod = p; sled = s; }
	};

	Elem *prvi, *posl, *tek, *preth;
	Elem *stari;

	List(){
		prvi = posl = tek = preth = 0;
		stari = 0;
	}

	~List(){ isprazni(); }

	void dodaj(PCB*& p){
		posl = (!prvi ? prvi : posl->sled) = new Elem(p);
	}

	void naPrvi() {
		tek = prvi; preth = 0;
	}
	void naSled() {
		preth = tek; if (tek) tek = tek->sled;
	}
	int imaTek() const {
		if ( tek != 0 ) return 1;
		else return 0;
	}

	PCB*& dohvTekPod(){ //metoda bi trebalo da se koristi samo tamo gde je provereno da tek nije null
		return tek->pod;
	}

	void izbaciTek(){
		if (!tek) return;
		stari = tek;
		tek = tek->sled;
		(!preth ? prvi : preth->sled) = tek;
		if (!tek) posl = preth;
		delete stari;
	}

	void isprazni();

};

#endif
