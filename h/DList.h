#ifndef _dlist_h_
#define _dlist_h_

#include "PCB.h"
typedef unsigned int Time;

class DList {
public:

	struct Elem {
		PCB* pod;
		Time vreme;
		Elem* sled;
		Elem (PCB*& p, Time t, Elem* s = 0)
			{ pod = p; vreme = t; sled = s; }
	};

	Elem *prvi, *posl, *tek, *preth;
	Elem *stari, *novi, *p, *q;

	DList() {
		prvi = posl = tek = preth = 0;
		stari = novi = p = q = 0;
	}
	~DList(){ isprazni(); }

	void dodaj(PCB*& p, Time t);

	void naPrvi() {
		tek = prvi; preth = 0;
	}
	void naSled() {
		preth = tek; if (tek) tek = tek->sled;
	}
	int imaTek(){
		if ( tek != 0 ) return 1;
		else return 0;
	}

	PCB*& dohvTekPod(){ //metoda bi trebalo da se koristi samo tamo gde je provereno da tek nije null
		return tek->pod;
	}

	void izbaciTek(){
		if (!tek) return;
		stari = tek;
		tek = tek->sled;
		(!preth ? prvi : preth->sled) = tek;
		if (!tek) posl = preth;
		delete stari;
	}

	void isprazni();

};

#endif
