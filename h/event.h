#ifndef _event_h_
#define _event_h_

#include "IVTEntry.h"

typedef unsigned char IVTNo;
class KernelEv;

typedef int ID; //dodato

class Event {
public:
	Event (IVTNo ivtNo);
	~Event ();

	void wait ();

protected:

	friend class KernelEv;
	void signal(); // can call KernelEv

private:
	ID id;
};

#endif
