#ifndef _system_h_
#define _system_h_

#include "PCB.h"
#include "KernThr.h"
#include "idle.h"
#include "semaphor.h"
#include "event.h"
#include "AList.h"
#include "DList.h"

#define lock asm cli
#define unlock asm sti

void interrupt timer(...);
void interrupt SysCallRout(...);
void interrupt change(...);

typedef unsigned char IVTNo;
typedef void interrupt (*pInterrupt)(...);
typedef enum { nothing, creatingKernelThread, destroyingKernelThread, creatingIdle } Special;

typedef enum { ThrDispatch, ThrConst, ThrDest, ThrStart, ThrWTC, ThrSleep, SemConst,
	SemDest, SemWait, SemSignal, SemVal, EvConst, EvDest, EvWait } Name;

typedef struct SysData{
	Name name;
	ID id;
	StackSize stSize;
	Time time;
	int in; //init, toBlock
	int ret; //return from wait, val
	IVTNo numEnt;
	Thread* thr;
	Semaphore* sem;
	Event* ev;
} SystemData;

class System {
public:
	static PCB* running;
	static KernelThread* runningKernelThread;
	static PCB* runningKernelPCB;

	static Idle* idle;
	static PCB* idlePCB;
	static PCB* startPCB;
	static DList* listaUspavanih;

	static SystemData* SData;
	static AList* PCBs;
	static AList* KernelSems;
	static AList* KernelEvs;

	static Special special;
	static unsigned int lockFlag;
	static unsigned int zahtevana_promena_konteksta;
	static int brojac;
	static pInterrupt oldRout;

	static void init_System();
	static void restore_System();

};

#endif
