#ifndef _PCB_h_
#define _PCB_h_

#include "list.h"
typedef int ID;
typedef unsigned long StackSize;
typedef unsigned int Time;
class Thread;

typedef enum { NOV, SPREMAN, BLOKIRAN, ZAVRSIO } Stanje;

class PCB{
public:
	unsigned *stek;
	List* listaBlokiranih;
	unsigned bp;
	unsigned sp;
	unsigned ss;

	Stanje stanje;
	unsigned beskonacno;
	int kvant;
	Thread* myThread;

	ID IDPCB;
	static ID posIDPCB;

	PCB (StackSize stackSize, Time timeSlice, Thread*& v);
	~PCB();

	void start();
	void waitToComplete();
	static void sleep(Time timeToSleep);
};

#endif
