#ifndef _idle_h_
#define _idle_h_

#include "thread.h"

class Idle :public Thread {
public:

	Idle (StackSize stackSize = defaultStackSize): Thread(stackSize, 1){};
	virtual void run();

};

#endif
