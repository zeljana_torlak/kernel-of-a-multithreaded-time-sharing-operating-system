#ifndef _testthr_h_
#define _testthr_h_

#include "thread.h"
#include "KernSem.h"
#include "KernEv.h"

class KernelThread: public Thread{
protected:
	virtual void run();
public:
	KernelThread(StackSize stackSize = defaultStackSize): Thread(stackSize, 0){} //TimeSlice?

	static PCB* a;
	static KernelSem* b;
	static KernelEv* c;
};

#endif
