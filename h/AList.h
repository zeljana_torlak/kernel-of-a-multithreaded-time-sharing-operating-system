#ifndef _alist_h_
#define _alist_h_

typedef int ID;
class Thread;

class AList {
public:

	struct Elem {
		void* pod;
		ID IDElem;
		Elem* sled;
		Elem (void*& p, ID d,  Elem* s = 0)
			{ pod = p; IDElem = d; sled = s; }
	};

	Elem *prvi, *posl, *tek, *preth;
	Elem *stari;

	AList(){
		prvi = posl = tek = preth = 0;
		stari = 0;
	}

	~AList(){ isprazni(); }

	void dodaj(void* p, ID d){ //bez &
		posl = (!prvi ? prvi : posl->sled) = new Elem(p,d);
	}

	void naPrvi() {
		tek = prvi; preth = 0;
	}
	void naSled() {
		preth = tek; if (tek) tek = tek->sled;
	}
	int imaTek() const {
		if ( tek != 0 ) return 1;
		else return 0;
	}

	void*& dohvTekPod(){ //metoda bi trebalo da se koristi samo tamo gde je provereno da tek nije null
		return tek->pod;
	}

	void izbaciTek(){
		if (!tek) return;
		stari = tek;
		tek = tek->sled;
		(!preth ? prvi : preth->sled) = tek;
		if (!tek) posl = preth;
		delete stari;
	}

	void isprazni();

	unsigned nadji(ID id);
	unsigned nadjiPCB(Thread*& t);

};

#endif
