#ifndef _kernev_h_
#define _kernev_h_

class PCB;
class Event;
typedef int ID;

typedef unsigned char IVTNo;

class KernelEv{
public:
	static KernelEv* kernelEvents[256];
	static ID posIDEv;
	ID IDEv;

	int value;
	PCB* blokiranPCB;
	PCB* vlasnik;

	IVTNo ivtNo;
	Event* myEvent;

	KernelEv (IVTNo N, Event*& e);
	~KernelEv ();

	void wait ();
	void signal();
};

#endif
