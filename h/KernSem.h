#ifndef _kernsem_h_
#define _kernsem_h_

#include "list.h"
typedef int ID;
class Semaphore;

class KernelSem {
public:
	int value;
	Semaphore* mySem;
	List* listaCekanja;

	static ID posIDSem;
	ID IDSem;

	KernelSem (int init, Semaphore*& s);
	~KernelSem ();

	int wait (int toBlock);
	void signal();

	int val () const;
};

#endif
