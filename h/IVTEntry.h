#ifndef _ivtentry_h_
#define _ivtentry_h_

#include "KernEv.h"

#define PREPAREENTRY(_n,_callOld)\
void interrupt inter##_n(...);\
IVTEntry newEnt##_n(_n,inter##_n);\
void interrupt inter##_n(...){\
	newEnt##_n.signal();\
	if(_callOld==1)newEnt##_n.callOld();}

typedef void interrupt (*pInterrupt)(...);
typedef unsigned char IVTNo;

class IVTEntry {
public:
	pInterrupt oldRoutine;
	IVTNo numEntry;

	IVTEntry(IVTNo numEntr, pInterrupt interruptRoutine);
	~IVTEntry();

	void callOld();
	void signal();
};

#endif
